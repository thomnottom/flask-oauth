import os
from flask import Flask, redirect, url_for, render_template
from flask_dance.consumer import OAuth2ConsumerBlueprint
 
app = Flask(__name__)
app.secret_key = "supersekrit"
pingfed_client_id = os.environ.get("OAUTH_CLIENT_ID")
pingfed_client_secret = os.environ.get("OAUTH_CLIENT_SECRET")
 
 
pingfed_blueprint = OAuth2ConsumerBlueprint(
    "pingfed-example", __name__,
    client_id=pingfed_client_id,
    client_secret=pingfed_client_secret,
    base_url="https://pfd-ciam-dev.mtb.com",
    token_url="https://pfd-ciam-dev.mtb.com/as/token.oauth2",
    authorization_url="https://pfd-ciam-dev.mtb.com/as/authorization.oauth2",
    scope = ['openid','profile'],
    redirect_to="auth"
)
app.register_blueprint(pingfed_blueprint, url_prefix="/login")
 
@app.route("/auth")
def auth():
    resp=pingfed_blueprint.session.get("/idp/userinfo.openid")
    assert resp.ok
    response = resp.content
    return render_template('auth.html', cont=response)
 
@app.route("/")
def login():
    if not pingfed_blueprint.session.authorized:
        return render_template("login.html")
    resp = pingfed_blueprint.session.get("/idp/userinfo.openid")
    assert resp.ok
    return "Here's the content of my response: " + resp.content
    #return "You are @{login} on PingFed".format(login=resp.json()["login"])
 
 
if __name__=='__main__':
    app.run(debug=True)
    